# Mitt program


Card: en klasse som representerer et enkelt kort. Kortet inneholder en type og en verdi. eks: type kløver verdi 8.
Metoder: getType(returner kort typen), getNumber(hvilken tall som kortet har). getValue(hvilken verdi kortet representerer) getString(en string sommensatt av tegn på kortet og tallet.)

Deck: Lager en full kortstokk bestående av 52. Lager en nøstet løkke som itererer over hverenkelt kortegn og legger til hvert kort innenfor tegnet i enn Arraylist. 
Metoder: metode som sjekker om stokken er tom. en som sjekker nåværende størrelse på kortstokken. En deler ut et kort, og trekker fra et kort fra kortstokken. 

Hand: Deler ut kort til brukeren. Tar kort fra korstokken og gir ut til brukeren. 
metoder: addCard(gir ut et kort til brukeren), getHandValue(returnerer verdien til kortene brukeren har på hånda), over21(sjekker om verdien på hånda er over 21.), getCards(viser kortene brukeren har på hånda.), toString(viser en streng med kortene brukeren har på hånda, samt den sammelagte verdien.)

Player: opretter en spiller. Spilleren må fylle inn et navn, og får tildelt en hånd. 
metoder: getName(returner navnet på spilleren), getHand(returnerer hånda til spilleren)

Game: inneholder selve spillet. Her kaller den på de andre classene, oppretter spiller, deler ut kort, og verifiserer ulovlige trekk og om hvorvidt spilleren vil trekke eller holde kortene sine. 
Metoder: startCards(deler ut to kort til spilleren ved opprettelse av spill.), playerTurn(spilleren avgjør hvorvidt han vil stå eller trekke nytt kort.) lookForWninner(se hvem som vinner),endGame(avsluttespillet.)

Visuals: Alle de visuelle effektene for spillet 

Hvordan spille spillet:

Ved opprettesle av spill får spilleren en 2 kort. i tilleg får han se et av kortene til motstanderen. Målet til spilleren er å få sine egne korts samlede verdi til å bli så nærme 21 som mulig, uten å gå over 21. Spilleren kan enten tryke på "hit" og trekke et nytt kort. Eller så kan spilleren trykke stand, og bli stående med kortene han har. Etter å ha trykket "stand" får spilleren se motstanderens resterende kort, og om hvorvidt han har vunnet.

Spilleren har start saldo på 1000$. Å kjøpe seg inn igjen etter en runde har tapt må han betale 100$. Dersom han blir blakk er spillet over


https://games.washingtonpost.com/games/blackjack // i based my visuals upon this game.