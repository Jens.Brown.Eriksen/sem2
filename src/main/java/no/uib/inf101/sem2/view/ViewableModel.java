 package no.uib.inf101.sem2.view;

import java.util.List;

 import no.uib.inf101.sem2.Blackjack.Hand;
import no.uib.inf101.sem2.Blackjack.Player;




public interface ViewableModel {
    /**
    Returns a list of the players participating in the game.
    @return A list of Player objects
    */
    List<Player> getPlayers();
    /**
    Returns the hand of the player.
    @return The player's Hand object
    */
    Hand getPlayerHand();
    /**
    Returns the hand of the dealer.
    @return The dealer's Hand object
    */
    Hand getDealerHand();
    /**
    Checks if the game is over.
    @return true if the game is over, false otherwise
    */
    boolean isGameOver();
    /**
    Checks if the player has lost.
    @return true if the player has lost, false otherwise
    */
    boolean playerLost();
    /**
    Adds a card to the player's hand and checks if the hand value is over 21.
    */
    void hit();
    /**
    Ends the player's turn and proceeds to the dealer's turn.
    */
    void stand();
    /**
    Checks if the player's turn has ended.
    @return true if the player's turn has ended, false otherwise
    */
    boolean isPlayerTurnEnded();
    /**
    Restarts the game.
    */
    void restart();
    /**
    Returns the player's balance as a string.
    @return The player's balance as a string
    */
    String getBalanceToString();
    /**
    Returns the player's balance as an integer.
    @return The player's balance as an integer
    */
    int getBalance();
    }