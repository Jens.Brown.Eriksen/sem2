package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseAdapter;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.List;
import javax.swing.JPanel;

import no.uib.inf101.sem2.Blackjack.Card;
import no.uib.inf101.sem2.Blackjack.Hand;
import no.uib.inf101.sem2.Blackjack.Player;

import java.awt.Font;


public class Visuals extends JPanel {   // Lager et visuals objekt og importerer alle bilder til brettet
  
  private ViewableModel model;
  private boolean mouseIsInTheRectangle = false;
  private boolean playerTurnEnded=false;
  BufferedImage logo = Inf101Graphics.loadImageFromResources("/brett.png");
  BufferedImage chip = Inf101Graphics.loadImageFromResources("/chip.png");
  BufferedImage grønn = Inf101Graphics.loadImageFromResources("/grønn.png");
  BufferedImage grønn1 = Inf101Graphics.loadImageFromResources("/grønn1.png");
  BufferedImage hit = Inf101Graphics.loadImageFromResources("/hit.png");
  BufferedImage stand = Inf101Graphics.loadImageFromResources("/stand.png");
  BufferedImage reset = Inf101Graphics.loadImageFromResources("/blackChip.png");
  BufferedImage gameOver = Inf101Graphics.loadImageFromResources("/Broke.Jpeg");
  private Rectangle2D Reset= new Rectangle2D.Double(getWidth() - 220, getHeight() / 2 + 50, 100, 100);
  
  private Rectangle2D Hit = new Rectangle2D.Double(150, getHeight() / 2 - 30, 100, 60);
  private Rectangle2D Stand = new Rectangle2D.Double(getWidth() - 220, getHeight() / 2 - 30, 100, 60);


  
  
  public Visuals(ViewableModel model) {
    this.setPreferredSize(new Dimension(900, 640));
    this.setupMousePositionUpdater();
    this.setupMousePressedUpdater();
    this.model= model;
    
  }
  public void setModel(ViewableModel model){
    this.model=model;
  }
  public void setPlayerTurnIsEnded(boolean isEnded){
    this.playerTurnEnded= isEnded;


  }
  public void drawDealerScore(Graphics2D g2){     // tegner Scoren til dealeren
    Hand dealerHand= model.getDealerHand();
    int handValue= dealerHand.getHandValue();
    String value= String.valueOf(handValue);
    g2.setColor(Color.WHITE);
    g2.setFont(new Font("Arial", Font.BOLD, 30));
    g2.drawString(value, getWidth()/2-280, getHeight()/2-150);


  }
  public void drawPlayerScore(Graphics2D g2){     //Tegner score til spilleren
    Hand playerHand= model.getPlayerHand();
    int handValue= (playerHand.getHandValue());
    String value=String.valueOf(handValue);
    g2.drawImage(chip,getWidth()/2-300,getHeight()/2+120,80,80,null);
    g2.setColor(Color.WHITE);
    g2.setFont(new Font("Arial", Font.BOLD, 30));
    g2.drawString(value, getWidth()/2-280, getHeight()/2+170);

  }
  public void drawDealerInitialCards(Graphics2D g2){      // tegner hånda til dealeren. Et av kortene er skjult
    BufferedImage hiddenCard=Inf101Graphics.loadImageFromResources("/bakside.png");
    Hand dealerHand= model.getDealerHand();
    List<Card> cards= dealerHand.getCards();
    int x= 350;
    int y=50;
    int overlapp= 30;
    g2.drawImage(hiddenCard,x,y,130,180,null);
    for (Card card: cards){
      
      String imageLink= "";
      String cardString=card.toString();
      if(cardString.startsWith("Hjerter")){
        imageLink+="H";
      }
      else if(cardString.startsWith("Ruter")){
        imageLink+="R";
      }
      else if(cardString.startsWith("Kløver")){
        imageLink+="K";
      }
      else if( cardString.startsWith("Spar")){
        imageLink+="S";
      }
      imageLink +=card.getNumber()+".png";
      BufferedImage kort=Inf101Graphics.loadImageFromResources("/"+imageLink);
      g2.drawImage(kort,x+overlapp,y,130,180,null);

    }
    



  }
  public void drawDealerCard(Graphics2D g2){    // tegner alle kortene til dealeren
    Hand dealerHand= model.getDealerHand();
    List<Card> cards= dealerHand.getCards();
    int x= 350;
    int y=50;
    int overlapp= 30;

    for (Card card: cards){
      
      String imageLink= "";
      String cardString=card.toString();
      if(cardString.startsWith("Hjerter")){
        imageLink+="H";
      }
      else if(cardString.startsWith("Ruter")){
        imageLink+="R";
      }
      else if(cardString.startsWith("Kløver")){
        imageLink+="K";
      }
      else if( cardString.startsWith("Spar")){
        imageLink+="S";
      }

      imageLink +=card.getNumber()+".png";
      BufferedImage kort=Inf101Graphics.loadImageFromResources("/"+imageLink);
      g2.drawImage(kort, x, y, 130, 180, null);
      x+=overlapp;

      
    }

  }
  public void drawPlayerCard(Graphics2D g2){      // Tegner kortene til spilleren
    Hand playerHand= model.getPlayerHand();
    List<Card> cards= playerHand.getCards();
    int x= 350;
    int y= 400;
    int overlapp= 30;

    

    for (Card card: cards){
      
      String imageLink= "";
      String cardString=card.toString();
      if(cardString.startsWith("Hjerter")){
        imageLink+="H";
      }
      else if(cardString.startsWith("Ruter")){
        imageLink+="R";
      }
      else if(cardString.startsWith("Kløver")){
        imageLink+="K";
      }
      else if( cardString.startsWith("Spar")){
        imageLink+="S";
      }
      imageLink +=card.getNumber()+".png";
      BufferedImage kort=Inf101Graphics.loadImageFromResources("/"+imageLink);
      g2.drawImage(kort, x, y, 130, (180), null);
      x+=overlapp;

      
    }

  }
  public void buyIn(Graphics2D g2){       // tegner knappen for å starte ny runde
    g2.drawImage(reset, getWidth() - 220, getHeight() / 2 + 50, 100, 100, null);
    g2.setFont(new Font("Arial", Font.BOLD, 15));
    g2.drawString("Put in your bet:", 550, 420);
    g2.setColor(Color.ORANGE);
    g2.drawString("$100",710,422);
  }
  public void drawBalance(Graphics2D g2){
    
    g2.setColor(Color.WHITE);
    g2.setFont(new Font("Arial", Font.BOLD, 30));
    g2.drawString("Balance: $"+model.getBalanceToString(),600,150);
    

  }
  
  
  @Override
  protected void paintComponent(Graphics g) {     // tegner spillet
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    Hit.setRect(150, getHeight() / 2 - 30, 100, 60);
    Stand.setRect(getWidth() - 220, getHeight() / 2 - 30, 100, 60);
    Reset.setRect(getWidth() - 220, getHeight() / 2 + 50, 100,100);

   


    // Calculate the scale factor while maintaining aspect ratio
    double scaleX = (double) getWidth() / logo.getWidth();                  //*Copied Chatgpt directly when placing the "brett.png" */
    double scaleY = (double) getHeight() / logo.getHeight();
    double scale = Math.min(scaleX, scaleY);

    // Calculate the new width and height of the image
    int newWidth = (int) (logo.getWidth() * scale);
    int newHeight = (int) (logo.getHeight() * scale);

    // Calculate the centered position of the image
    int x = (getWidth() - newWidth) / 2;
    int y = (getHeight() - newHeight) / 2;

    
    g2.drawImage(grønn1, x, y, newWidth, newHeight, null);
    g2.drawImage(chip,getWidth()/2-300,getHeight()/2-200,80,80,null);
    g2.drawImage(chip,getWidth()/2-300,getHeight()/2+120,80,80,null);
    g2.drawImage(hit,150,getHeight()/2-30,100,60,null);
    g2.drawImage(stand,getWidth()-220,getHeight()/2-30,100,60,null);
    drawBalance(g2);
    drawPlayerCard(g2);
    
     if(playerTurnEnded){
      buyIn(g2);
      drawDealerCard(g2);
      drawDealerScore(g2);

    Player player = model.getPlayers().get(0);
    Player dealer = model.getPlayers().get(1);
    int playerHandValue = player.getHand().getHandValue();
    int dealerHandValue = dealer.getHand().getHandValue();

      if(model.playerLost()){
        g2.drawString("The dealer wins", getWidth()/2-100, getHeight()/2);
      }
      else if (playerHandValue==dealerHandValue){
        g2.drawString("It's a draw", getWidth() / 2 - 100, getHeight() / 2);
      }
      else {
        g2.drawString("You win!", getWidth()/2-100, getHeight()/2);
      }
     }
     else {
      drawDealerInitialCards(g2);
     }
    drawPlayerScore(g2);

    if(model.getBalance()<0){
      g2.drawImage(gameOver, x, y, newWidth, newHeight, null);
      


    }
    
    }

  private Rectangle2D getRectangle() {
    return new Rectangle2D.Double(50, 50, getWidth() - 100, getHeight() - 100);
  }

  private void setupMousePositionUpdater() {
    // Keep the mousePosition variable up to date
    this.addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        mouseIsInTheRectangle = getRectangle().contains(e.getPoint());
        updateCursor();
        repaint();
      }
    });
  }

  private void updateCursor() {
    if (mouseIsInTheRectangle) {
      setCursor(new Cursor(Cursor.HAND_CURSOR));
    } else {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }
  private void setupMousePressedUpdater() {         //føler hvor spileren trykker på skjermen, og starter funksjonene ettersom hvor spilleren trykker.
    this.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {

        if (Reset.contains(e.getPoint())) {
          model.restart();
          repaint();
          return;
        }
        

        
        if (model.isPlayerTurnEnded()){
          
          return;
        }
        if(Hit.contains(e.getPoint())){
          model.hit();
        }
        else if (Stand.contains(e.getPoint())){

         model.stand();
        }

        repaint();
      }
  
      @Override
      public void mouseReleased(MouseEvent e) {
        repaint();
      }
    });
  }

}

