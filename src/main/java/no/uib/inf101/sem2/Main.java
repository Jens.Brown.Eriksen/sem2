package no.uib.inf101.sem2;

import no.uib.inf101.sem2.Blackjack.Game;
import no.uib.inf101.sem2.view.Visuals;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Main {
  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> {   // innførte denne linjen ved hjelp av chatgpt.
      
      
    
      Visuals visuals=new Visuals(null);
      Game game = new Game("Player", visuals);
      visuals.setModel(game);
      game.startGame();

      // Create the main window (JFrame)
      JFrame frame = new JFrame("Blackjack");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.add(visuals);
      frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);
  });
}

}
