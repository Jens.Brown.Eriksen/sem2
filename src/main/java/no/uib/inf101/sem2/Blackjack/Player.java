package no.uib.inf101.sem2.Blackjack;



// Oppretter en spiller 

public class Player {
    private String playerName;
    private Hand hand;

    public Player(String playerName){
        this.playerName=playerName;
        this.hand=new Hand();

    }
    public String getName(){   // henter navnet på spilleren
        return playerName;

    }
    public Hand getHand(){ //       Henter hånda på spilleren
        return hand;
        
    }

    
}
