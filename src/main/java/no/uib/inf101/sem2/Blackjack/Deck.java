package no.uib.inf101.sem2.Blackjack;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

// Klassen samler alle kortene til en kortstokk

public class Deck {
    private List<Card> cards;
    

    public Deck(){                      // Lager en kortstokk og stokker den
        cards= new ArrayList<>();
        for(int itype=0; itype<Card.xType.length; itype++){
            for (int inumber=0; inumber<Card.xNumber.length;inumber++){
                cards.add( new Card(itype, inumber));
            }

            Collections.shuffle(cards);
        }
    
        
    }
    public void restack(){                  //Stokker kortstokken
        cards.clear();
        for (int itype = 0; itype < Card.xType.length; itype++) {
            for (int inumber = 0; inumber < Card.xNumber.length; inumber++) {
                cards.add(new Card(itype, inumber));
            }
        }
        Collections.shuffle(cards);
    }
    public int deckSize(){          //returner størrelsen på kortstokken
        return cards.size();
    }

    public boolean emtyDeck(){          //Sjekker om kortstokken er tom.
        if (cards.size() < 1){
            return true;
        }
        else
            return false; 
    }
    public Card dealCard(){             // Trekker fra et kort i kortstokken
        
        if (this.emtyDeck()){
            throw new IllegalStateException("Kortstokken er tom");
        }
        return cards.remove(cards.size()-1);
    }


}