package no.uib.inf101.sem2.Blackjack;

import java.util.ArrayList;
import java.util.List;

//Lager en hånd for spilleren og en for dealer.

public class Hand {
    private List<Card> cards;

    public Hand(){                  //lager en hånd
        cards= new ArrayList<>();

    }
    public void addCard( Card card){    //Legger til et kort på hånda
        cards.add(card);

    }
    public int getHandValue(){      // hentur ut samlet verdi av hånda. Håndtere også ess, som kan ha 2 forskjellige verdier
        int handValue=0;
        int aces=0;
        for (Card card:cards){
            handValue+=card.getValue();
            if( card.getNumber()=="1"){
                aces++;
            }
        
        }
        while(handValue<=11 && aces >0){
            handValue+=10;
            aces--;

            
        }
        return handValue;
    }
    public boolean over21(){        // sjekker om verdien på hånda er over 21

        if (getHandValue()>21){
            return true;
        }
        else
            return false;
    }
    public List<Card> getCards(){       // Henter ut kortene på hånda
        return new ArrayList<>(cards);
    }
    
    public String toString(){
        return cards.toString()+ " "+ getHandValue();
    }
    public void clearHand(){            // Sletter kortene på hånda
        cards.clear();
    }




}
