package no.uib.inf101.sem2.Blackjack;




// Klassen lager alle kortene som kna eksistere i en kortstokk

public class Card {                                 
    public static final String[] xType ={"Kløver","Hjerter","Spar","Ruter"};
    public static final String[] xNumber={"2","3","4","5","6","7","8","9","10","11","12","13","1"};
    public static final int[] xValues= {2,3,4,5,6,7,8,9,10,10,10,10,1};
    
    
    
    private int value;
    private String type;
    private String number;

    public Card(int itype, int inumber){
        this.type= xType[itype];
        this.number=xNumber[inumber];
        this.value= xValues[inumber];


    }
    public int getValue(){ //Returner verdien til et enkelt kort
        return value;
    }
    public String getNumber(){ //Returnerer hvilket tall som står på kortet
        return number;
    }
    public String getType(){ //returner hvilken type kortet er. kløver, hjerter, tuer eller spar.
        return type;
    }
    
    public String toString(){   // lager en String med typen og tallet til kortet.
        return type+" "+ number;
    }
}
