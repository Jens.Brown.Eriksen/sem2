package no.uib.inf101.sem2.Blackjack;
import java.util.ArrayList;
import java.util.List;
import no.uib.inf101.sem2.view.ViewableModel;
import no.uib.inf101.sem2.view.Visuals;

public class Game implements ViewableModel{ // Lager et nytt spill objekt
    private Deck deck;
    private List<Player> players;
    private boolean youLost;
    
    private boolean playerTurnEnded;
    private Visuals visuals;
    private int balance;
    private boolean gameOver;
    

    
    public Game(String Name, Visuals visuals){
        
        deck= new Deck();
        players = new ArrayList<>();
        players.add(new Player("Player"));
        players.add(new Player("Dealer"));
        youLost= false;
        playerTurnEnded=false;
        this.visuals=visuals;
        balance= 1000;
        gameOver=false;
        
        
    }
    public void startCards(){                   // Deler ut 2 kort til hver spiller i starten av spillet
        for (Player player: players){
            player.getHand().addCard(deck.dealCard());
            player.getHand().addCard(deck.dealCard());

        }
    }
    public void hit(){                          // Spilleren trekker et nytt kort til hånda si.
        Player player= players.get(0);
        if (deck.emtyDeck()){
            deck.restack();
        }
        player.getHand().addCard(deck.dealCard());
    
        
        if(player.getHand().over21()){
           
            youLost=true;
            playerTurnEnded=true;
            visuals.setPlayerTurnIsEnded(true);
            visuals.repaint();
           
        }
    }
    public void stand(){                // Spiller trekker ikke kort. Dealern starter sin tur, og en vinner kåres
        
        playerTurnEnded=true;
        visuals.setPlayerTurnIsEnded(true);
        visuals.repaint();
        
        dealerTurn();
        countScore();
    }
    
    
 
    public void dealerTurn(){                   // Dealeren trekker kort fram til verdien på hånda er over 17
        Player dealer= players.get(1);

        while (dealer.getHand().getHandValue()<17){
            if (deck.emtyDeck()){
                deck.restack();
            }
            dealer.getHand().addCard(deck.dealCard());
        }
    
    }
    public void countScore(){                               //Teller resultatet og kårer en vinner
        Player player= players.get(0);
        Player dealer= players.get(1);

      

        if (dealer.getHand().over21()|| dealer.getHand().getHandValue()< player.getHand().getHandValue()){
            
            youLost=false;
            balance+=200;
        }
        else if (dealer.getHand().getHandValue()==player.getHand().getHandValue()){
           
            youLost=false;
            balance+=100;
        }
       
        else {
            
            youLost=true;
            
        }
        


    }
    public void startGame(){        //Starter et nytt blackjack spill
        startCards();

        if (youLost==false){
            dealerTurn();
            
        }
    }
    @Override
    public List<Player> getPlayers() {
        return players;
    }
    @Override
    public Hand getPlayerHand() {

        return players.get(0).getHand();
    }
    @Override
    public Hand getDealerHand() {
        return players.get(1).getHand();

    }

    @Override
    public boolean playerLost() {
        return youLost;
    }


    @Override
    public boolean isGameOver() {
        
        return gameOver;
    }
    @Override
    public boolean isPlayerTurnEnded(){
        return playerTurnEnded;
    }
    @Override
    public int getBalance(){
        return balance;
    }
    @Override
    public String getBalanceToString(){
        return Integer.toString(balance);
    }

    public void restart() {         //Nullstiller spillfunksjoner for å starte på nytt
        if (balance >=0 ) {
            for (Player player : players) {
                player.getHand().clearHand();
            }

            youLost = false;
            playerTurnEnded = false;
            visuals.setPlayerTurnIsEnded(false);

            balance -= 100;
            System.out.println(balance);
            startGame();
        } else {
            gameOver = true;
            
        }
    }



}
