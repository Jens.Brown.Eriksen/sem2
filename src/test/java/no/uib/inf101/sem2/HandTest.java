package no.uib.inf101.sem2;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.Blackjack.Card;
import no.uib.inf101.sem2.Blackjack.Hand;

import static org.junit.jupiter.api.Assertions.*;

public class HandTest {
    @Test
    void getHandValue() {
        Hand hand = new Hand();
        hand.addCard(new Card(0, 0)); // 2 of Kløver
        hand.addCard(new Card(1, 1)); 
        assertEquals(5, hand.getHandValue());       // opretter en hånd og deler ut kløver 2 og hjerter 3. sjekker at verien er 5
    }  

    @Test
    void over21() {
        Hand hand = new Hand();
        hand.addCard(new Card(2, 9));       //opretter ny hånd, legger til tre kort av verdi 10. sjekker at detter er over 21.
        hand.addCard(new Card(3, 9)); 
        hand.addCard(new Card(1, 9));
        assertTrue(hand.over21());
    }
}
