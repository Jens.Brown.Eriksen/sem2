package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.Blackjack.Card;


// Alle testene oppretter et kløver 2 kort, og sjekker om dette blir oprettet.

public class CardTest {
  @Test
  void getValue() {
      Card card = new Card(0, 0); 
      assertEquals(2, card.getValue());
  }

  @Test
  void getNumber() {
      Card card = new Card(0, 0); 
      assertEquals("2", card.getNumber());
  }

  @Test
  void getType() {
      Card card = new Card(0, 0); 
      assertEquals("Kløver", card.getType());
  }
}
