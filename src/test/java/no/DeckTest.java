package no;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.Blackjack.Deck;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {
    @Test
    void deckSize() {
        Deck deck = new Deck();  
        assertEquals(52, deck.deckSize()); //Opretter en ny kortstokk, og sjekker at størrelsen på stokken er 52.
    }

    @Test
    void emptyDeck() {
        Deck deck = new Deck();             //Oppretter ny kortstokk, deler ut alle kortene og sjekker at kortstokken er tom.
        assertFalse(deck.emtyDeck());
        for (int i = 0; i < 52; i++) {
            deck.dealCard();
        }
        assertTrue(deck.emtyDeck());
    }
}


